import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const SAVE_FORM = 'SAVE_FORM';

export default new Vuex.Store({
  strict: true,
  state: {
    firstName: '',
  },
  mutations: {
    [SAVE_FORM](state, payload) {
      this.state.firstName = payload.firstName;
    },
  },
  actions: {
    submitForm({ commit }, payload) {
      commit('SAVE_FORM', payload);
    },
  },
  getters: {
    getFirstName: state => state.firstName,
  },
});
